﻿using UnityEngine;

public interface IMapCreate
{
	GameObject Water { set; get; }
	GameObject Brick { set; get; }
	GameObject Concreate { set; get; }
	GameObject Tower { set; get; }
	GameObject TowerBig { set; get; }
	GameObject Floor { set; get; }
	
	GameObject Tank { set; get; }

	GameObject[,] Create(string file);

	Map Card { set; get; }
}
