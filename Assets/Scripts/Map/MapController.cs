﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Conformity
{
	public string id;
	public GameObject gameObject;
}

public class MapController : MonoBehaviour 
{
	public string path;

	private IMapCreate _mapCreate;
	private GameObject[,] _card;

	public GameObject water;
	public GameObject brick;
	public GameObject concreate;
	public GameObject tower;
	public GameObject towerBig;
	public GameObject floor;

	public GameObject tank;

	private float _time = 0;
	private float _timeNextBonus;
	public float maxTimeInterval = 50;

	public GameObject[] bonuses;

	void Start () 
	{
		_mapCreate = new MapCreateJson();
		_mapCreate.Water = water;
		_mapCreate.Brick = brick;
		_mapCreate.Concreate = concreate;
		_mapCreate.Tower = tower;
		_mapCreate.TowerBig = towerBig;
		_mapCreate.Floor = floor;
		_mapCreate.Tank = tank;

		_card = _mapCreate.Create(Application.dataPath + "/../" + path);
		
		_timeNextBonus = Random.Range(0, maxTimeInterval);

		GameController.IsGame = true;
	}
	
	void Update () 
	{
		_time += Time.deltaTime;
		if(_time > _timeNextBonus)
		{
			_timeNextBonus = Random.Range(0, maxTimeInterval);
			_time = 0;

			int i = 0; 
			int j = 0; 

			while(_card[i, j] != null)
			{
				i = Random.Range(0, _card.GetLength(0));
				j = Random.Range(0, _card.GetLength(1));
			}

			Instantiate(bonuses[Random.Range(0, bonuses.Length)],
				new Vector3((j - _mapCreate.Card.width / 2f) * _mapCreate.Card.titleSize, 
				(i - _mapCreate.Card.height / 2f) * _mapCreate.Card.titleSize), new Quaternion());
		}
	}
}
