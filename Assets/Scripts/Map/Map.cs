﻿using System;

public class Map
{
	public int height;
	public int width;
	public float titleSize;
	public Cell[] cells;

	public override string ToString()
	{
		string str = "";
		str = "height = " + height.ToString() +
			"\nwidth = " + width.ToString() +
			"\ntitleSize = " + titleSize.ToString() +
			"\nCells: ";
		if(cells != null)
			for(int i = 0; i < cells.Length; i++)
			{
				str += "{ id = " + cells[i].id.ToString() + " direction = " +  cells[i].direction.ToString() + " x = " 
					+ cells[i].x.ToString() + " y = " + cells[i].y.ToString() + " }";
			}

		return str;
	}
}
[Serializable]
public struct Cell
{
	public string id;
	public DirectionMovement direction;
	public int x;
	public int y;
}
