﻿using UnityEngine;
using System.IO;

public class MapCreateJson : MonoBehaviour, IMapCreate
{
	public GameObject Water { set; get; }
	public GameObject Brick { set; get; }
	public GameObject Concreate { set; get; }
	public GameObject Tower { set; get; }
	public GameObject TowerBig { set; get; }
	public GameObject Floor { set; get; }

	public GameObject Tank { set; get; }

	public Map Card { set; get; }

	public GameObject[,] Create(string file)
	{
		return CreateMap(JsonToMap(file));
	}

	private Map JsonToMap(string file)
	{
		string filePath = Path.Combine(Application.streamingAssetsPath, file);

        if(File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            Card = JsonUtility.FromJson<Map>(dataAsJson);
        }
        else
        {
            Debug.LogError("Cannot load game data!");
        }

		return Card;
	}

	private GameObject[,] CreateMap(Map map)
	{
		GameObject[,] mapGameObject = new GameObject[map.height, map.width];

		for(int i = 0; i < map.cells.Length; i++)
		{
			switch (map.cells[i].id)
			{
				case "brick":
					mapGameObject[map.cells[i].y, map.cells[i].x] = 
						Instantiate(Brick, new Vector3((map.cells[i].x - map.width / 2f) * map.titleSize, 
							(map.cells[i].y - map.height/2f)  * map.titleSize),
							Quaternion.Euler(0, 0, (int)map.cells[i].direction));
					break;
				case "water":
					mapGameObject[map.cells[i].y, map.cells[i].x] = 
						Instantiate(Water, new Vector3((map.cells[i].x - map.width / 2f) * map.titleSize, 
							(map.cells[i].y - map.height/2f)  * map.titleSize),
							Quaternion.Euler(0, 0, (int)map.cells[i].direction));
					break;
				case "concreate":
					mapGameObject[map.cells[i].y, map.cells[i].x] = 
						Instantiate(Concreate, new Vector3((map.cells[i].x - map.width / 2f) * map.titleSize, 
							(map.cells[i].y - map.height/2f)  * map.titleSize),
							Quaternion.Euler(0, 0, (int)map.cells[i].direction));
					break;
				case "tower":
					mapGameObject[map.cells[i].y, map.cells[i].x] = 
						Instantiate(Tower, new Vector3((map.cells[i].x - map.width / 2f) * map.titleSize, 
							(map.cells[i].y - map.height/2f)  * map.titleSize),
							Quaternion.Euler(0, 0, (int)map.cells[i].direction));
					break;
				case "towerBig":
					mapGameObject[map.cells[i].y, map.cells[i].x] = 
						Instantiate(TowerBig, new Vector3((map.cells[i].x - map.width / 2f) * map.titleSize, 
							(map.cells[i].y - map.height/2f)  * map.titleSize), 
							Quaternion.Euler(0, 0, (int)map.cells[i].direction));
					break;
				case "tank":
					Instantiate(Tank, new Vector3((map.cells[i].x - map.width / 2f) * map.titleSize, 
						(map.cells[i].y - map.height/2f)  * map.titleSize), 
						Quaternion.Euler(0, 0, (int)map.cells[i].direction));
					break;
			}	
		}
		for(int i = 0; i < map.height; i++)
		{
			for(int j = 0; j < map.width; j++)
			{
				Instantiate(Floor, new Vector3((j - map.width / 2f) * map.titleSize, (i - map.height / 2f) * map.titleSize), new Quaternion());
			}
		}
		return mapGameObject;
	}
}
