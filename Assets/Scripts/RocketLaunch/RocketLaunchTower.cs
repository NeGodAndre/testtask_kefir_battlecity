﻿using UnityEngine;

public class RocketLaunchTower : RocketLaunch 
{
	public enum Side { one, four };
	public Side _side;

	public string tagPlayer = "Player";

	private Transform _target;

	void Start()
	{
		Initialization();
	}

	void FixedUpdate () 
	{
		_time += Time.deltaTime;

		if(_target != null)
		{
			if(_time > timeMax)
			{
				switch(_side)
				{
					case Side.one:
						if(CheckedShot(transform.position + transform.up * indent, transform.up))
							Shot(transform.position + transform.up * indent, transform.rotation);
						_time = 0;
						break;
					case Side.four:
						if(CheckedShot(transform.position + transform.up * indent, Vector3.up))
							Shot(transform.position + Vector3.up * indent, Quaternion.AngleAxis(0, Vector3.forward));

						if(CheckedShot(transform.position + Vector3.left * indent, Vector3.left))
							Shot(transform.position + Vector3.left * indent, Quaternion.AngleAxis(90, Vector3.forward));

						if(CheckedShot(transform.position + Vector3.down * indent, Vector3.down))
							Shot(transform.position + Vector3.down * indent, Quaternion.AngleAxis(180, Vector3.forward));

						if(CheckedShot(transform.position + Vector3.right * indent, Vector3.right))
							Shot(transform.position + Vector3.right * indent, Quaternion.AngleAxis(270, Vector3.forward));
						_time = 0;
						break;
				}
			}
		}
		else
		{
			GameObject temp = GameObject.FindGameObjectWithTag(tagPlayer);
			if(temp != null)
			{
				_target = temp.transform;
			}
		}
	}

	private bool CheckedShot(Vector3 origin, Vector3 direction)
	{
		RaycastHit2D hit = Physics2D.Raycast(origin, direction);
		return hit.transform == _target;
	}
}
