﻿using UnityEngine;

public class RocketLaunchTank : RocketLaunch 
{
	public float timeRechargeFastMax = 10f;
	private float _timeRechargeFast;
	private bool _isRechargeFast = false;

	void Start()
	{
		Initialization();
	}
	
	void FixedUpdate () 
	{
		_time += Time.deltaTime;
		_timeRechargeFast += Time.deltaTime;
		if(GameController.IsGame)
		{
			if(Input.GetAxis("Jump") != 0 && _time > timeMax)
			{
				Shot(transform.position + transform.up * indent, transform.rotation);
				_time = 0;
			}
			if(_isRechargeFast && _timeRechargeFast > timeRechargeFastMax)
			{
				_isRechargeFast = false;
				_timeRechargeFast = 0;
				timeMax *= 2;
			}
		}
	}

	void TakeBonus(BonusType nameBonus)
	{
		if(nameBonus == BonusType.RechargeFast)
		{
			_isRechargeFast = true;
			_timeRechargeFast = 0f;
			timeMax /= 2;
		}
	}
}
