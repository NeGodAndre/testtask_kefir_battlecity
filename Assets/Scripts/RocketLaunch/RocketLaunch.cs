﻿using System.Collections.Generic;
using UnityEngine;

public class RocketLaunch : MonoBehaviour 
{
	public float indent = 0.64f;
	public float timeMax = 2f;

    protected float _time = 0f;

	private List<BulletScript> _bullets;
	public GameObject bullet;
	protected Vector2 _positionBullet;

	protected void Initialization()
	{
		_bullets = new List<BulletScript>();
	}

	protected void Shot(Vector3 positionSpam, Quaternion rotation)
	{
		for(int i = 0; i < _bullets.Count; i++)
		{
			if(!_bullets[i].gameObject.activeSelf)
			{
				_bullets[i].gameObject.SetActive(true);
				_bullets[i].gameObject.transform.position = positionSpam;
				_bullets[i].gameObject.transform.rotation = rotation;
				return;
			}
		}
		_bullets.Add(Instantiate(bullet, positionSpam, rotation).GetComponent<BulletScript>());		
	}
}
