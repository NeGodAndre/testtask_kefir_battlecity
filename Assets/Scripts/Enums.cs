﻿public enum DirectionMovement
{
	Up = 0,
	Right = 90,
	Down = 180,
	Left = 270
}

public enum BonusType
{
	HealthPlus,
	Defence,
	RechargeFast
}