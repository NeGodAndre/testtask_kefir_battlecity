﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthTank : MonoBehaviour 
{
	public int health = 4;

	public float timeDefence = 5f;
	private float _time = 0f; 
	private bool _isDefence = false;
	
	private Sprite _spriteDefolt;
	public Sprite spriteDefence;
	private SpriteRenderer _spriteRender;

	void Start () 
	{
		_spriteRender = GetComponent<SpriteRenderer>();
		_spriteDefolt = _spriteRender.sprite;
		GameController.NumberTanks++;
	}
	
	void Update () 
	{
		_time += Time.deltaTime;
		if(_time > timeDefence && _isDefence)
		{
			_isDefence = false;
			_spriteRender.sprite = _spriteDefolt;
		}
	}

	void TakeDamage()
	{
		if(!_isDefence)
		{
			health--;

			if(health <= 0)
			{
				Destroy(gameObject);
				GameController.NumberTanks--;
			}
		}
	}

	void TakeBonus(BonusType nameBonus)
	{
		switch (nameBonus)
		{
			case BonusType.Defence:
				_isDefence = true;
				_time = 0;
				_spriteRender.sprite = spriteDefence;
				break;
			case BonusType.HealthPlus:
				health += 4;
				break;
		}
	}
}
