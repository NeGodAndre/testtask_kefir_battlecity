﻿using UnityEngine;

public class BonusScript : MonoBehaviour 
{
	public BonusType bonus;

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player")
		{
			other.gameObject.SendMessage("TakeBonus", bonus, SendMessageOptions.DontRequireReceiver);
			Destroy(gameObject);
		}
	}
}
