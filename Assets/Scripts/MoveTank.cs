﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTank : MonoBehaviour 
{
	public float speed = 1;

	private Rigidbody2D _rigidbody;

	void Start () 
	{
		_rigidbody = GetComponent<Rigidbody2D>();
	}
	
	
	void FixedUpdate () 
	{
		if(GameController.IsGame)
		{
			if(Input.GetAxis("Horizontal") != 0)
			{
				float sign = Mathf.Sign(Input.GetAxis("Horizontal"));
				Vector2 newPosition = transform.position;
				newPosition.x += sign * speed * Time.deltaTime;
				_rigidbody.MovePosition(newPosition);
				_rigidbody.MoveRotation(90 * -sign);
			}
			if(Input.GetAxis("Vertical") != 0)
			{
				float sign = Mathf.Sign(Input.GetAxis("Vertical"));
				Vector2 newPosition = transform.position;
				newPosition.y += sign * speed * Time.deltaTime;
				_rigidbody.MovePosition(newPosition);
				if(sign == 1)
					_rigidbody.MoveRotation(0);
				else
					_rigidbody.MoveRotation(180);
			}
		}
	}
}
