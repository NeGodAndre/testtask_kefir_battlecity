﻿using UnityEngine;

public class Health : MonoBehaviour 
{
	public int health = 4;

	void Start () 
	{
		GameController.NumberTowers++;
	}
	
	void Update () 
	{
		
	}

	void TakeDamage()
	{
		health--;

		if(health <= 0)
		{
			Destroy(gameObject);
			GameController.NumberTowers--;
		}
	}
}
