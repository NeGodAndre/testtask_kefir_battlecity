﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickDestroy : MonoBehaviour 
{
	public GameObject[] bricks;
	private int _numberBrick;

	void Start () 
	{
		_numberBrick = bricks.Length;
	}
	
	void Update () 
	{
		
	}

	void TakeDamage()
	{
		if(_numberBrick > 0)
		{
			_numberBrick--;
			bricks[_numberBrick].SetActive(false);
		}
		if (_numberBrick <= 0)
			Destroy(gameObject);
	}
}
