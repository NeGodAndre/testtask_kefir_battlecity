﻿using UnityEngine;

public class BulletScript : MonoBehaviour 
{
	public float speed;
	public float TimeLife = 10f;
	private float _time;
	private Rigidbody2D _rigidbody;

	void Start () 
	{
		_rigidbody = GetComponent<Rigidbody2D>();
	}
	
	void FixedUpdate () 
	{
		Vector2 newPosition = transform.position;
		newPosition += (Vector2)transform.up * speed * Time.deltaTime;
		_rigidbody.MovePosition(newPosition);
		_time += Time.deltaTime;
		if(_time > TimeLife)
		{
			_time = 0;
			gameObject.SetActive(false);
		}
	}
		
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag != "Bonus")
		{
			other.gameObject.SendMessage("TakeDamage", SendMessageOptions.DontRequireReceiver);
			gameObject.SetActive(false);
		}
	}

	public void NewPosition(Vector2 newPosition)
	{
		_rigidbody.position = newPosition;
	}
}