﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour 
{
	public static int NumberTowers { set; get; }
	public static int NumberTanks { set; get; }
	public static bool IsGame { set; get; }

	public Image panel;
	public Text text;

	void Awake () 
	{
		IsGame = false;
	}
	
	void Update () 
	{
		if(IsGame)
		{
			if(NumberTanks == 0)
			{
				panel.gameObject.SetActive(true);
				text.text = "You lose";
			}
			if(NumberTowers == 0)
			{
				panel.gameObject.SetActive(true);
				text.text = "You win";
			}
		}
	}
}
